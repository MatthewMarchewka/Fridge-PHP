-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 14 Lut 2018, 22:10
-- Wersja serwera: 10.1.30-MariaDB
-- Wersja PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `fridgedatabase`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `collectiontable`
--

CREATE TABLE `collectiontable` (
  `user` text COLLATE utf8_polish_ci NOT NULL,
  `pozx` int(11) NOT NULL,
  `pozy` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `zindex` int(11) NOT NULL,
  `text` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `collectiontable`
--

INSERT INTO `collectiontable` (`user`, `pozx`, `pozy`, `width`, `height`, `zindex`, `text`) VALUES
('glosnik', 88, 88, 200, 160, 1, 'Hello <b>world</b>!'),
('glosnik', 315, 26, 200, 160, 2, 'Hello <b>world</b>!'),
('glosnik', 88, 88, 200, 160, 1, 'Hello <b>world</b>!'),
('jbl', 257, 74, 869, 199, 3, '<p>Hello <strong>world</strong>! mefikon<br></p>'),
('jbl', 30, 74, 200, 160, 2, 'Hello <b>world</b>!'),
('jbl', 504, 75, 200, 160, 1, 'Hello <b>world</b>!'),
('\"@#\' ąść;\"', 480, 55, 200, 160, 1, 'Hello <b>world</b>!'),
('\"@#\' ąść;\"', 781, 43, 200, 160, 3, 'Hello <b>world</b>!'),
('\"@#\' ąść;\"', 256, 44, 200, 160, 2, 'Hello <b>world</b>!');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `usertable`
--

CREATE TABLE `usertable` (
  `nick` text COLLATE utf8_polish_ci NOT NULL,
  `history` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `usertable`
--

INSERT INTO `usertable` (`nick`, `history`) VALUES
('glosnik', 3),
('jbl', 3),
('\"@#\' ąść;\"', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
